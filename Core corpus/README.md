**FRENCH VERSION**

*Introduction*

- La plupart des textes du corpus est distribuée sous licence CC BY-NC-SA 4.0 (Attribution / NonCommercial / ShareAlike / 4.0 Internal) <https://creativecommons.org/licenses/by-nc-sa/4.0/>. Certains textes cependant, toujours protégés par le droit d'auteur, ne peuvent cependant être partagés librement dans ce répertoire. Lorsque le projet MICLE n'a pas l'autorisation de partager les textes, nous ne communiquerons ceux-ci qu'avec leurs métadonnées, sans les mots-formes, afin d'autoriser la recherche, ou bien en contexte limité, sur le portail TXM-CRISCO <https://txm-crisco.huma-num.fr/txm/>, selon l'art. L122-5CPI de la législation française, de laquelle dépend l'hébergement du projet.

*Corpus Gold*

- La version Gold du corpus MICLE <https://www.unicaen.fr/projet_de_recherche/micle/> est composée de X textes ancien français (de Normandie et anglo-normand) et de X textes en ancien vénitien. Chaque texte a son propre dossier du type "DATE_Nom", dans lequel se trouve différents sous-dossiers par format d'encodage.

*Annotation de la partie française*

- La partie française du corpus a été annotée automatiquement par HOPS <https://github.com/hopsparser/hopsparser> (Grobl & Benoît, 2021, https://hal.archives-ouvertes.fr/hal-03223424/file/HOPS_final.pdf). L'annotation a été corrigée manuellement pour les UPOS, et les lemmes ont été annotées grâce aux dictionnaires PRESTO <https://presto.ens-lyon.fr/> et AND <https://anglo-norman.net/>. L'étiquetage UD (<https://universaldependencies.org/format.html>) a été ensuite converti dans les formats PRESTO et UPENN <https://www.ling.upenn.edu/hist-corpora/annotation/index.html>. Les outils ayant servi à la conversion sont disponibles dans le dossier correspondant. Les trois jeux d'étiquettes (UD, UPENN, PRESTO) autorisent différents degrés d'analyse et leur combinaison permet d'affiner les résultats des questions de recherche.

*Annotation de la partie vénitienne*

- La partie vénétienne a été annotée manuellement, en l'absence de documentation suffisante pour cette langue, directement dans le système UPENN. Les étiquettes ont ensuite été converties selon le modèle UD pour servir de futur modèle d'entraînement, avec quelques indications syntaxiques. Cette partie du travail durera jusqu'à la fin du projet MICLE, et nous communiquerons ici les données d'entraînement pour les futurs projets les exploitant.

*Édition numérique*

- Pour l'édition numérique des textes, les choix de transcriptions et de découpages des phrases, merci de vous rendre sur la page de documentation générale sur le site du projet : <https://www.unicaen.fr/projet_de_recherche/micle/> ou sur le portail TXM-Crisco <https://txm-crisco.huma-num.fr/txm/>.

*Formats de fichier*

- La version XML-TEI est la version "base" des textes du corpus. Elle comporte toutes les informations métalinguistiques que l'on retrouve dans les autres formats, avec un *header* détaillant l'ensemble des caractéristiques de l'édition et du texte concerné. Merci de vous y reporter en cas de questions sur l'établissement des textes. Pour faciliter leur navigation, les XML ont été encodés avec les niveaux book/chapter/section/paragraph/sentence/word, chacun numéroté continuement et avec réinitialisation du compteur à chaque nouvel élément parent. Dans les cas où le texte n'a qu'un niveau de structure, celui-ci a été considéré comme une @section, et nous avons rajouté des divisions @chapter et @book "vides" pour conserver une hiérarchisation homogène de tous les textes du corpus.

- Les versions CONLLU, POS et éventuellement PSD des textes ont été générées à partir de cette version base. Des informations relatives au projet MICLE sont présentes en entête, avec le titre, langue et date du texte concerné. Également, la numérotation des phrases dans ces versions {Sentence X-X-X-X-X} (POS) #sent_id="X-X-X-X-X" (CONLLU) correspondent au XPath de la phrase dans l'XML-TEI. Par exemple, la phrase "1-27-1-6-1" correspond à la première phrase du sixième paragraphe de la première section du vingt-septième chapitre du premier libre. Cette numérotation, homogène entre toutes les versions du fichier, facilite la recherche.

*Accès via TXM-CRISCO*

- La version XML-TEI du corpus a été versée sur le portail TXM-CRISCO <https://txm-crisco.huma-num.fr/txm/> dans le dossier "MICLE", pour permettre la lecture au kilomètre (exception faite des textes protégés par le droit d'auteur, voir *supra*) et la recherche grâce aux requêtes CQL <https://www.sketchengine.eu/documentation/corpus-querying/>.

*Annotation syntaxique*

- Le CONLL et l'XML-TEI proposent une analyse syntaxique en dépendance des phrases du corpus. Les versions PSD et leurs conversion en XML-TEI proposent une analyse en constituants sur une partie des textes : la numérotation des phrases et le XPATH sont identiques avec les versions complètes, pour faciliter leur recherche. Au regard de la structure hiérarchique de l'XML-TEI "base", on trouve deux éléments supplémentaires, \<cl\> (pour "proposition") et \<phr\> ("syntagme"), avec différents attributs. L'XML-TEI "Part/Parsed" a été versé à part sur le portail TXM-CRISCO pour ne pas interférer avec les recherches dans le texte intégral.

*Informations complémentaires*

- Pour les détails regardant les jeux d'étiquettes et les formats employés pour ce corpus, merci de visiter les différents liens présents ici.

----

**ENGLISH VERSION**

*Introduction*

- Most of the texts in the corpus are distributed under the CC BY-NC-SA 4.0 (Attribution / NonCommercial / ShareAlike / 4.0 Internal) license <https://creativecommons.org/licenses/by-nc-sa/4.0/>. Some texts, however, are still protected by copyright and cannot be shared freely in this directory. When the MICLE project does not have permission to share the texts, we will only communicate them with their metadata, without the words, in order to allow research, or in limited context, on the TXM-CRISCO portal <https://txm-crisco.huma-num.fr/txm/>, according to art. L122-5CPI of the French legislation, on which the hosting of the project depends.

*Gold corpus*

- The Gold version of the MICLE <https://www.unicaen.fr/projet_de_recherche/micle/> corpus is composed of 4 Old French texts (from Normandy and Anglo-Norman) and 5 Old Venetian texts. Each text has its own folder of type "DATE_Nom", in which there are different subfolders by encoding format.

*French annotation*

- The French part of the corpus was automatically annotated by HOPS <https://github.com/hopsparser/hopsparser> (Grobl & Benoît, 2021, https://hal.archives-ouvertes.fr/hal-03223424/file/HOPS_final.pdf). The annotation was manually corrected for UPOS, and the lemmas were annotated using PRESTO <https://presto.ens-lyon.fr/> and AND <https://anglo-norman.net/> dictionaries. The UD tagset (<https://universaldependencies.org/format.html>) was then converted into PRESTO and UPENN <https://www.ling.upenn.edu/hist-corpora/annotation/index.html> formats. The tools used for the conversion process will be available in the corresponding folder. The three tagsets (UD, UPenn and Presto) are used for the French corpus as each offers a slightly different level of analysis and combining them permits finetuning queries depending on the research question.

*Venetian annotation*

- The Venetian part was annotated manually, in the absence of sufficient documentation for this language, directly in the UPENN system. The labels were then converted to the UD model to serve as a future training model, with some syntactic indications. This part of the work will last up until the end of the MICLE project, and we will communicate here the training data for future projects using them.

*Digital edition*

- For digital editing of the texts, choices of transcriptions and sentence breakdowns, please check the documentation page on the main website: <https://www.unicaen.fr/projet_de_recherche/micle/>.

*File format*

- The XML-TEI version is the "main" version of the texts in the corpus. It includes all the metalinguistic information found in the other formats, with a *header* detailing all the characteristics of the edition and the text concerned. Please refer to it if you have any questions about the editing and encoding of the texts. For ease of navigation, the XML has been encoded with the levels book/chapter/section/paragraph/sentence/word, each continuously numbered and with a resetted counterat each new parent element. In cases where the text has only one level of structure, it was considered as a @section, and we added "empty" @chapter and @book divisions to keep a homogeneous hierarchy of all the texts of the corpus.

- The CONLLU, POS and eventually PSD versions of the texts were generated from this main version. Informations about the MICLE project is included in the header, with the title, language and date of the text. Also, the sentence numbering in these versions {Sentence X-X-X-X-X} (POS) #sent_id="X-X-X-X-X" (CONLLU) corresponds to the XPath in the XML-TEI. For example, the sentence "1-27-1-6-1" corresponds to the first sentence of the sixth paragraph of the first section of the twenty-seventh chapter of the first book of this specific text. This numbering, homogeneous between all the versions of the file, facilitates the search.

*Access via TXM-CRISCO*

- The XML-TEI version of the corpus has been uploaded to the TXM-CRISCO portal <https://txm-crisco.huma-num.fr/txm/> in the "MICLE" folder, to allow reading by the mile (except for copyrighted texts, see *supra*) and searching through the use of the CQL language <https://www.sketchengine.eu/documentation/corpus-querying/>.

*Syntactic parsing*

- The CONLL and the main XML-TEI files offer a dependencies analysis on the sentences of the corpus. The PSD versions and their conversions into XML-TEI propose a constituent analysis. The numbering of the sentences and the XPATH are identical with the main versions, to facilitate their research. With regard to the hierarchical structure of the "base" XML-TEI, there are two additional elements, \<cl\> (for "clause") and \<phr\> ("phrase"), with different attributes. The XML-TEI "Part/Parsed" has been placed separately on the TXM-CRISCO portal so as not to interfere with full text searches.

*Further info*

- For details regarding the tag sets and formats used for this corpus, please visit the various links here.