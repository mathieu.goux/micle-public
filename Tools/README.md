**MICLE-FR** / V. 1.1., May 2024.

----
----

**VERSION FRANÇAISE**

----

Ce dossier regroupe les outils et scripts utilisés par l'équipe pour structurer et corriger la version française du corpus MICLE. Une grande partie des scripts correspondants ont été développés par Rayan Ziane dans le cadre de son travail sur le projet [High-Tech](https://crisco.unicaen.fr/recherche/projet-rin-high-tech-1089578.kjsp?RH=1531402918899) et sont disponibles sur son [Github](https://github.com/RZiane/HT_CRISCO).

----

**ENGLISH VERSION**

----

This folder contains the tools and scripts used by the team to structure and correct the French version of the MICLE corpus. Many of the corresponding scripts were developed by Rayan Ziane as part of his work on the [High-Tech](https://crisco.unicaen.fr/recherche/projet-rin-high-tech-1089578.kjsp?RH=1531402918899) project and are available on his [Github](https://github.com/RZiane/HT_CRISCO).