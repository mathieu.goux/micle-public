**MICLE-FR** / V. 1.1., May 2024.

----
----

**VERSION FRANÇAISE**

----

**INFORMATIONS GÉNÉRALES**

**Site institutionnel**

<https://www.unicaen.fr/projet_de_recherche/micle/>

**Description du projet**

*Projet collaboratif entre l’université de Caen Normandie (France) et Goethe Universität Frankfurt am Main (Allemagne).*

Le projet MICLE (*MICro-indicateurs de L’Évolution grammaticale : un modèle multifactoriel de la perte de V2 en italien et en français anciens*) se propose d’accomplir les objectifs principaux suivants :

- constituer un corpus de textes des mêmes types pour le français de Normandie et la langue vénitienne, des origines au 17e siècle ;
- d’expliquer le changement syntaxique dans la forme de la phrase dans ces deux variétés romanes pour la  période investiguée ;
- d’élaborer un modèle reliant le changement grammatical à l’acquisition du  langage, pour faire avancer les pratiques méthodologiques et les perspectives conceptuelles dans le domaine.
	
Dates : 01/06/2021 - 31/05/2024

Financement : ANR-DFG (<https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-20-fral-0001/>)

**Corpus**	

Ayant pour but de développer et de tester un modèle multifactoriel de l’évolution de l’ordre des mots, le projet MICLE élabore un corpus bilingue entre deux variétés romanes ayant subi des développements comparables, le vénitien et le français de Normandie.

Le corpus des textes vise à permettre de se rapprocher de la langue de tous les jours à travers le temps. Allant des plus anciens témoins jusqu’au 17e siècle, ces textes appartiennent aux mêmes deux types non-littéraires, ce qui permet de minimiser les distorsions que créerait l’assemblage de textes divers. Les types retenus sont :

- les pièces de procès ;
- les correspondances personnelles et d’affaires

parce qu’ils sont susceptibles de contenir des témoignages et des traces de dialogues.  Pourtant, nous ne négligeons pas les spécificités des textes de chaque langue. Le corpus vénitien comprendra notamment des statuts et autres textes juridiques qui éclairent le système du gouvernement dans les différentes régions sous le contrôle vénitien ; le corpus français, pour sa part, donnera de l’importance aux procès en sorcellerie en Normandie, depuis Jeanne d’Arc jusqu’à Madeleine Bavent.

Numérisés et balisés en suivant les préceptes de la TEI (Text Encoding Initiative), lemmatisés et annotés afin d’optimiser la recherche, les textes seront mis à la disposition des chercheurs et du public sur le site du projet, actuellement en cours de construction, et sur ce serveur GIT.

**Équipe**

- Porteurs du projet: Pierre Larrivée (CRISCO · université de Caen Normandie · France) & Cécilia Poletto (Institut für Romanische Sprachen und Literaturen · Goethe Universität · Frankfurt, Germany)

- Post-doctorants du projet: Mathieu Goux (CRISCO · université de Caen Normandie · France), Francesco Pinzin (Institut für Romanische Sprachen und Literaturen · Goethe Universität · Frankfurt, Germany) & Natlia Romanova (CRISCO · université de Caen Normandie · France)

- Stagiaires (2022) : Agathe Aubert, Lucy Marie-Leblanc, Marie Picart & Valentin Simenel (Unicaen).

----

**ENGLISH VERSION**

----

**BASIC INFORMATION**

**Institutional website**

<https://www.unicaen.fr/en/projet_de_recherche/micle-2/>

**Project description**

*A collaborative project between the University of Caen (France) and Goethe University, Frankfurt am Main (Germany).*

The MICLE (*Micro-cues of language evolution: A Multifactorial model of V2 loss in Central Romance*) project aims to :

- create a corpus of texts of the same types for the Norman French and the Venetian language, for the origins to the seventeenth century
- explain syntactic change in the sentence structure in two Romance language varieties for the period in question
- advance methodological practices and conceptual perspectives in the field by creating a model linking grammatical change to the acquisition of language.
	
Dates : 06/01/2021 - 05/31/2024

Funders : ANR-DFG (<https://anr.fr/en/funded-projects-and-impact/funded-projects/project/funded/project/b2d9d3668f92a3b9fbbf7866072501ef-a55ba403b3/>)

**Corpus**	

In order to achieve MICLE’s main goal of elaborating and testing a multifactorial model of word order evolution, we are creating a bilingual corpus of texts in two Romance language varieties with simiar trajectories of development, the Venetian and the French of Normandy.

Our corpus aims, as far as possible, to give acces to the everyday language from the earliest vernacular witnesses to the seventeenth century. The texts of the corpus belong to the same two non-literary genres, which will allow us to avoid distortions and biases that collecting a diverse range of texts would have introduced. The genres in question are :

- legal texts and trial accounts;
- personal and business correspondence

since they are likely to contain evidence of spoken language and traces of dialogue. At the same time, we take into account the specificity of both languages’ extant witnesses. The Venetian part of the corpus will thus include statutes and other legal texts that illuminate the system of government in different regions under Venetian control. On the other hand, the French corpus will showcase witch trials in Normandy, from Jeanne d’Arc to Madelein Bavent.

Digitised and tagged following the rules of TEI (Text Encoding Initiative), lemmatised and annotated in order to enable the search function, the texts of the corpus are available to researchers and the public via the project website, currently under construction, and this GIT server.

**Team**

- Principal investigators: Pierre Larrivée (CRISCO · université de Caen Normandie · France) & Cécilia Poletto (Institut für Romanische Sprachen und Literaturen · Goethe Universität · Frankfurt, Germany)

- Post-doctoral researchers: Mathieu Goux (CRISCO · université de Caen Normandie · France), Francesco Pinzin (Institut für Romanische Sprachen und Literaturen · Goethe Universität · Frankfurt, Germany) & Natlia Romanova (CRISCO · université de Caen Normandie · France)

- Student interns (2022) : Agathe Aubert, Lucy Marie-Leblanc, Marie Picart & Valentin Simenel (Unicaen).